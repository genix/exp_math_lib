#include <stdlib.h>         // for calloc and friends 
#include <stdio.h>          //EOF etc
#include <string.h>         // memcpy
#include <math.h>

//numbers are stored low->high order 0101 is decimal 10
struct _number_struct  {
  char *bin;
  char *dec;
  int blen;
  int dlen;
} ;

typedef enum {
    E_SHIFTINDEX = -2,
    E_STRINDEX = -3,
    E_SHIFT = -4,
} Errcode;



#define E_SUCCESS 0;
#define ceil(n)    ((n)+4* ((n)/3))
#define REG_SIZE    4

char *bcd_decimal_val(char *bcd,int no_registers); //we know each reg is 4 bits


typedef char t_bitstring;
typedef signed int e_value;
typedef struct _number_struct exp_n;

int    bs_printfhtol(exp_n *n, int line_length);
int     bs_printfltoh(t_bitstring *, int index);

exp_n *initialize(t_bitstring *s);


//use blen instead of epositiom  
t_bitstring *alg_doubledabble(exp_n *exp_number);


static int bs_shiftleft(t_bitstring *scratch, int index);
static int bs_getlen(t_bitstring *);
static int reg_getvalue(char *reg);
static int reg_setvalue(char *r, int value);
void print_errors(int e);



