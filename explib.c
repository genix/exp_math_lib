#include "explib.h"



//store the lengths of both binary and decimal strings and the strings
//themselves
char * alg_doubledabble(exp_n *exp_number)
{
	int scratch_size = ceil(exp_number->blen)+4;  
	t_bitstring *scratch = calloc(scratch_size, sizeof(char));
	*(scratch+scratch_size) = EOF;                            
	int v; 
	char *start = scratch+exp_number->blen;
	char *cur = start;
	char *last = start+REG_SIZE;                         
	char *ret;
        

  
  if ( *(exp_number->bin+exp_number->blen) != EOF )
	{
  	return  NULL;           
	}
        
	memcpy(scratch, exp_number->bin, exp_number->blen);
	
	for (int bitno=0; bitno< exp_number->blen; bitno++)
	{
		bs_shiftleft(scratch, scratch_size);      
		
		if (bitno == exp_number->blen-1)
		{
			break;
		}

   	if (reg_getvalue(last) > 0)
		{         
			last+=REG_SIZE;
		}

    while(cur != last)
		{

    	if((v = reg_getvalue(cur)) >= 5)
			{
      	reg_setvalue(cur, v+3);
      }
    	cur += REG_SIZE;
   	
		}
 	  cur = start;
  }

  if ((v = reg_getvalue(last)) > 0)
  {  
	  last+=REG_SIZE;
  }
	
  int no_of_registers, register_bits;

	for (register_bits=0; last != start ;register_bits++)
  {
    last--;
  }    
		
  no_of_registers = register_bits/4;
  
  ret = calloc(register_bits+1, 1);                                   //must have EOF in string
  
  memcpy(ret, scratch+exp_number->blen, register_bits);                   //copy all the bcd digits into the beginning of new string
	
  *(ret+register_bits) = EOF;                                     //delimit the string

  char *decimal_value = bcd_decimal_val(ret, no_of_registers);  //get the string with the decimal value
exp_number->dec = decimal_value;
exp_number->dlen = no_of_registers;

  //bs_printfhtol(decimal_value, no_of_registers);              //print out the value to the screen



  free(ret);                                                // cleanup c++ idiots find hard
	free(scratch);
	return decimal_value;                                   //return the string to calling function
}







char *bcd_decimal_val(char *bcd,  int no_registers)
{
	int dec_count, bin_count;
  char *p = bcd; 
  char *decimal_string = (char *)calloc(no_registers+1, 1); // +1 for the end of string

  for ((dec_count = 0), (bin_count = no_registers); (dec_count < no_registers); (dec_count++, bin_count--) )
	{
  	*(decimal_string+dec_count) = reg_getvalue(p);   
  	p+=4;
  }
  	*(decimal_string+dec_count) = EOF;
		return decimal_string;
}








static int bs_shiftleft(t_bitstring *string,int index){
    
int count;
int lastdigit = index-1;

t_bitstring *bit = string+lastdigit;
     
for(count=0; count < lastdigit; count++){
*bit = *(bit-1);
bit--;
}

*string = 0;
return E_SUCCESS;
}







int bs_printfltoh(t_bitstring *string, int index)
{   
int count;

if (*(string+index) != EOF ) return E_STRINDEX;
for(count=0;count < index; count++){
printf("%d", *string++);
}
printf("\n");
return 0;
}






//print to stdout the decimal value stored in ns
// Sufficiently large numbers will wrap the screen whem printing
// if line_length is set to zero or left altogether, tue function will
// wrap to a default of 80 chars. However line_length can be set to any
// value thebuser wishes. Value is printed to screen with LOB to left with
// increasing larger bits to tje right.


int bs_printfhtol(exp_n *ns, int line_length)
{

  char *digit = ns->dec;
  int lastdigit = ns->dlen-1;
  int newline = 1;  
  int breakline = (line_length == 0) ? (newline = 80) : (newline = line_length);
  
  if (*(digit+ns->dlen) != -1)
    return -1; 

  for (int count = lastdigit ; (count > -1) ; (count--))
  {
    if (newline == breakline)
    {
      printf("\n");
      newline = 1;
    }
	  printf("%d", *(digit+count));
	  newline++;
  } 
  printf("\n");
  return 0;
}








void print_errors(int e)
{
printf("died on error %d\n", e);
}







static int bs_getlen(t_bitstring *string){
t_bitstring *current_pos = string;
int string_len = 0;
while(*(current_pos++) != EOF)
{
string_len++;    
}
    
return string_len;
}







static int reg_getvalue(char *reg)
{
char *p = reg;
int regvalue = 0;

for(int counter=0;counter < REG_SIZE; counter++)
{
if(*(p+counter) )
{
regvalue+= pow(2, counter);
}
}

return regvalue;
}


static int reg_setvalue(char *r, int value)
{  
	int count;
    
	for(count=0;value>0;count++)
	{
		*(r+count) = value % 2;
		value /= 2;
	}
}

